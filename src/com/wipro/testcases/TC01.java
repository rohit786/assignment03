package com.wipro.testcases;

import java.io.*;
import java.util.*;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC01 {
	WebDriver dr;
	
	@BeforeClass
	public void startApp() {
		System.setProperty("webdriver.chrome.driver", "D:\\RO20111746\\QET_CoE_SeleniumL2\\resources\\driverfiles\\chromedriver.exe");
		dr = new ChromeDriver();
	}
	
	@AfterClass
	public void closeApp() {
		dr.close();
	}
	
	@Test
	public void step01() throws FileNotFoundException, IOException, InterruptedException {
		Properties properties=new Properties();
        properties.load(new FileInputStream("D:\\RO20111746\\QET_CoE_SeleniumL2\\resources\\config\\Config.properties"));
        String url = properties.getProperty("url");
        dr.get(url);dr.manage().window().maximize();
        Thread.sleep(2000);
	}
	
	@Test
	public void step02() throws InterruptedException{
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a")).click();
		dr.findElement(By.linkText("Register")).click();
		Thread.sleep(2000);
	}
	
	@DataProvider
	@Test
	public void step03() throws BiffException, IOException, InterruptedException  {
		File f =    new File("D:\\RO20111746\\QET_CoE_SeleniumL2\\testdata\\Registration.xls");
		//File f=new File("D:\\RO20111746\\seleniumTest01.xls");
        Workbook w=Workbook.getWorkbook(f);
        Sheet s= w.getSheet(0);
        
        int col=s.getColumns();
        int rows=s.getRows();
        
        String str[]=new String[col];
        
        for(int r=0;r<rows;r++)
        {
           for(int c=0;c<col;c++)
            {
                Cell ce=s.getCell(c,r);
                str[c]=ce.getContents();
               // System.out.print(str[c]+" ");
            }
            dr.findElement(By.name("firstname")).sendKeys(str[0]);
            dr.findElement(By.name("lastname")).sendKeys(str[1]);
            dr.findElement(By.name("email")).sendKeys(str[2]);
            dr.findElement(By.name("telephone")).sendKeys(str[3]);
            dr.findElement(By.name("password")).sendKeys(str[4]);
            dr.findElement(By.name("confirm")).sendKeys(str[5]);
            //dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/ul/li[5]/a")).click();
            Thread.sleep(2500);
        }

	}
	
	@Test
	public void step04() throws IOException, InterruptedException {
		 dr.findElement(By.name("agree")).click();
	     if(dr.findElement(By.name("agree")).isSelected()) {
	            BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"));
	            writer.write("Checkbox is checked.");
	            writer.close();
	     }
	     Thread.sleep(2500);
	}
	
	@Test
	public void step05() throws IOException {
		dr.findElement(By.xpath("//*[@id=\"content\"]/form/div/div/input[2]")).click();
        BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt", true));
        writer.write("\n"+dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
        File screenshotFile = ((TakesScreenshot)dr).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshotFile, new File("D:\\RO20111746\\QET_CoE_SeleniumL2\\screenshots\\TC01_5.png"));
        writer.close();
	}
	
	
	@Test
	public void step06() {
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/i")).click();
	}
	
	@Test
	public void step07() throws IOException, InterruptedException {
		dr.findElement(By.linkText("Logout")).click();
        BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt", true));
        writer.write("\n"+dr.findElement(By.xpath("//*[@id=\"content\"]/h1")).getText());
        writer.close();
        dr.close();
        Thread.sleep(2500);
	}
}
