package com.wipro.testcases;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class TC03 {
	WebDriver dr;
	
	@BeforeClass
	public void startApp() {
		System.setProperty("webdriver.chrome.driver", "D:\\RO20111746\\QET_CoE_SeleniumL2\\resources\\driverfiles\\chromedriver.exe");
		dr = new ChromeDriver();
	}
	
	@AfterClass
	public void closeApp() {
		dr.close();
	}
	
	@Test
	public void step01() throws FileNotFoundException, IOException, InterruptedException {
		Properties properties=new Properties();
        properties.load(new FileInputStream("D:\\RO20111746\\QET_CoE_SeleniumL2\\resources\\config\\Config.properties"));
        String url = properties.getProperty("url");
        dr.get(url);dr.manage().window().maximize();
        Thread.sleep(2000);
	}
	
	@Test
	public void step02() {
		dr.findElement(By.xpath("//*[@id=\"top-links\"]/ul/li[2]/a/i")).click();
		dr.findElement(By.linkText("Login")).click();
	}
	
	@DataProvider
	@Test
	public void step03() throws BiffException, IOException, InterruptedException {
		File file = new File("D:\\RO20111746\\QET_CoE_SeleniumL2\\testdata\\login.xls");
		Workbook w = Workbook.getWorkbook(file);
		Sheet s = w.getSheet(0);
		int col=s.getColumns();
        int rows=s.getRows();
        
        String str[]=new String[col];
        
        for(int r=0;r<rows;r++)
        {
           for(int c=0;c<col;c++)
            {
                Cell ce=s.getCell(c,r);
                str[c]=ce.getContents();
               // System.out.print(str[c]+" ");
            }
          
            dr.findElement(By.name("email")).sendKeys(str[0]);
            dr.findElement(By.name("password")).sendKeys(str[1]);
          
            Thread.sleep(2500);
        }
	}
	
	
	@Test
	public void step04() throws InterruptedException {
		dr.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/form/input")).click();
		Thread.sleep(2000);
	}
	
	@Test
	public void step05() throws InterruptedException {
		dr.findElement(By.linkText("Components")).click();
		dr.findElement(By.xpath("//*[@id=\"menu\"]/div[2]/ul/li[3]/div/div/ul/li[2]/a")).click();
		Thread.sleep(2000);
	}
	
	@Test
	public void step06() throws IOException {
		 BufferedWriter writer = new BufferedWriter(new FileWriter("output4.txt", true));
	     writer.write("\n"+dr.findElement(By.linkText("Apple Cinema 30\"")).getText());
	     writer.write("\n"+dr.findElement(By.xpath("//*[@id=\"content\"]/div[3]/div[1]/div/div[2]/div[1]/p[2]/span[1]")).getText());
	     writer.close();
	}
}
